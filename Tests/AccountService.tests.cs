using homework1.src;
using NUnit.Framework;
using System;

namespace Tests
{
    public class Tests
    {
        [Test]
        public void NoFirstName()
        {
            // Arrange
            var service = new AccountService("./src/Data/AccountsJson.txt");
            service.ClearAccounts();
            int accountsInit = service.GetNumberOfAccounts();

            // Act
            service.AddAccount(new Account { FirstName = "", LastName = "Gorokhovksii", BirthDate = new DateTime(1960, 12, 12) });

            // Assert
            Assert.IsTrue(service.GetNumberOfAccounts() == accountsInit, "�� ������ ���� ������� ������ ��� ���������� �����");
        }

        [Test]
        public void NoLastName()
        {
            // Arrange
            var service = new AccountService("./src/Data/AccountsJson.txt");
            service.ClearAccounts();
            int accountsInit = service.GetNumberOfAccounts();

            // Act
            service.AddAccount(new Account { FirstName = "Artem", LastName = "", BirthDate = new DateTime(1960, 12, 12) });

            // Assert
            Assert.IsTrue(service.GetNumberOfAccounts() == accountsInit, "�� ������ ���� ������� ������ ��� ���������� �������");
        }

        [Test]
        public void IncorrectDate()
        {
            // Arrange
            var service = new AccountService("./src/Data/AccountsJson.txt");
            service.ClearAccounts();
            int accountsInit = service.GetNumberOfAccounts();

            // Act
            service.AddAccount(new Account { FirstName = "Artem", LastName = "Gorokhovksii", BirthDate = DateTime.Now });

            // Assert
            Assert.IsTrue(service.GetNumberOfAccounts() == accountsInit, "�� ������ ���� ������� ������ ���� ��� 18 ���");
        }

        [Test]
        public void AllCorrect()
        {
            // Arrange
            var service = new AccountService("./src/Data/AccountsJson.txt");
            service.ClearAccounts();
            int accountsInit = service.GetNumberOfAccounts();

            // Act
            service.AddAccount(new Account { FirstName = "Artem", LastName = "Gorokhovksii", BirthDate = new DateTime(1960, 12, 12) });

            // Assert
            Assert.IsTrue(service.GetNumberOfAccounts() == accountsInit + 1, "������ ������ ���� ���������");
        }
    }
}