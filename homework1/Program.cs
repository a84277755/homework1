﻿using System;
using System.IO;
using homework1.src;
using Newtonsoft.Json;

namespace homework1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Part 1
            Console.WriteLine("Part 1:");
            var fileContent = File.ReadAllText("./src/Data/DataJson.txt");
            var serializer = new OtusJsonSerializer();

            var persons = new OtusStringReader<Person>(fileContent, serializer);

            foreach (var person in persons)
            {
                Console.WriteLine(person.Age);
            }


            // Part 2
            Console.WriteLine("Part 2:");
            var sortedPersons = new PersonSorter().Sort(persons);
            
            foreach(var person in sortedPersons)
            {
                Console.WriteLine(person.Age);
            }

            // Part 3
            Console.WriteLine("Part 3:");
            var repository = new Repository("./src/Data/AccountsJson.txt");
            repository.Add(new Account
            {
                BirthDate = new DateTime(),
                FirstName = "Oleg",
                LastName = "Petrov"
            });

            var oneAccount = repository.GetOne(Predicate1);
            if (oneAccount != null)
            {
                Console.WriteLine(oneAccount.FullName);
            }

            oneAccount = repository.GetOne(Predicate2);
            if (oneAccount != null)
            {
                Console.WriteLine(oneAccount.FullName);
            }

            repository.Add(new Account
            {
                BirthDate = new DateTime(),
                FirstName = "Artem",
                LastName = "Gorokhovskii"
            });

            repository.Add(new Account
            {
                BirthDate = new DateTime(),
                FirstName = "Nikolai",
                LastName = "Something"
            });

            oneAccount = repository.GetOne(Predicate1);
            if (oneAccount != null)
            {
                Console.WriteLine(oneAccount.FullName);
            }

            oneAccount = repository.GetOne(Predicate2);
            if (oneAccount != null)
            {
                Console.WriteLine(oneAccount.FullName);
            }

            var accounts = repository.GetAll();
            foreach(var account in accounts)
            {
                Console.WriteLine("> " + account.FirstName);
            }

            // check AccountService
            Console.WriteLine("===");

            var accountService = new AccountService("./src/Data/AccountsJson.txt");
            var someAccount = new Account
            {
                BirthDate = new DateTime(1960, 12, 12),
                FirstName = "",
                LastName = "Ivanov"
            };

            accountService.AddAccount(someAccount);

            oneAccount = repository.GetOne(Predicate3);
            if (oneAccount != null)
            {
                Console.WriteLine(oneAccount.FullName);
            }

            someAccount = new Account
            {
                BirthDate = new DateTime(1960, 12, 12),
                FirstName = "Petr",
                LastName = "Ivanov"
            };
            accountService.AddAccount(someAccount);

            accounts = repository.GetAll();
            foreach (var account in accounts)
            {
                Console.WriteLine("> " + account.FirstName);
            }
        }

        public static bool Predicate1 (Account account)
        {
            return account?.FirstName == "Artem";
        }

        public static bool Predicate2(Account account)
        {
            return account?.FirstName == "Oleg";
        }

        public static bool Predicate3(Account account)
        {
            return account?.LastName == "Ivanov";
        }
    }
}
