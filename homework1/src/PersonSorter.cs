﻿using homework1.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace homework1.src
{
    class PersonSorter : IAlgorithm<Person>
    {
        public IEnumerable<Person> Sort(IEnumerable<Person> notSortedItems)
        {
            return from i in notSortedItems
                              orderby i.Age
                              select i;
        }
    }
}
