﻿using homework1.src.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace homework1.src
{
    class OtusStringReader<T> : IEnumerable<T>
    {
        private readonly string data;
        private readonly ISerializer serializer;

        public OtusStringReader(string data, ISerializer serializer)
        {
            this.data = data;
            this.serializer = serializer;
        }

        public IEnumerator<T> GetEnumerator()
        {
            var items = serializer.Deserialize<T[]>(data);
            if (items == null)
            {
                items = Array.Empty<T>();
            }
            foreach (var item in items)
                yield return item;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

    }
}
