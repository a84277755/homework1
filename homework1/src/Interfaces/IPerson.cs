﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework1.src.Interfaces
{
    interface IPerson
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public string City { get; set; }
    }
}
