﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework1.src.Interfaces
{
    interface ISerializer
    {
        public string Serialize<T>(T item);
        public T Deserialize<T>(string data);
    }
}
