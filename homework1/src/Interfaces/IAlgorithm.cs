﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework1.src.Interfaces
{
    interface IAlgorithm<T>
    {
        public IEnumerable<T> Sort(IEnumerable<T> notSortedItems);

    }
}
