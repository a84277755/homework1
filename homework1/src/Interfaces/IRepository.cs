﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework1.src.Interfaces
{
    interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
    }
}
