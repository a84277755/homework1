﻿using homework1.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace homework1.src
{
    public class AccountService : IAccountService
    {
        private readonly string filePath;
        public AccountService(string filePath)
        {
            this.filePath = filePath;
        }
        public void AddAccount(Account account)
        {
            var namesAreCorrect = account.FirstName != "" && account.LastName != "";
            var totalDays = DateTime.Now.Subtract(account.BirthDate).TotalDays;

            // That's not so accurate, error in ~4.5 days may be
            var ageIsCorrect = (totalDays / 18) >= 365;

            if (ageIsCorrect && namesAreCorrect)
            {
                var repository = new Repository(filePath);
                repository.Add(account);
            }
        }

        public int GetNumberOfAccounts()
        {
            var repository = new Repository(filePath);
            return repository.GetAll().Count();
        }

        public void ClearAccounts()
        {
            var repository = new Repository(filePath);
            repository.Clear();
        }
    }
}
