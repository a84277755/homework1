﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework1.src
{
    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        public string FullName
        {
            get { return FirstName + " - " + LastName; }
        }
    }
}
