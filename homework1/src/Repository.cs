﻿using homework1.src.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace homework1.src
{
    class Repository : IRepository<Account>
    {
        private readonly string filePath;
        public Repository(string filePath)
        {
            this.filePath = filePath;
        }
        public void Add(Account item)
        {
            var accounts = GetAll();

            var result = new Account[accounts.Where(x => x != null).Count() + 1];

            var i = 0;
            foreach (var account in accounts)
            {
                if (account != null)
                {
                    result[i++] = account;
                }
            }
            result[i] = item;

            var serializer = new OtusJsonSerializer();
            string json = serializer.Serialize(result);
            File.WriteAllText(filePath, json);
        }

        public IEnumerable<Account> GetAll()
        {
            var data = File.ReadAllText(filePath);
            var serializer = new OtusJsonSerializer();
            var accounts = new OtusStringReader<Account>(data, serializer);
            return accounts.Where(x => x != null);
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            var accounts = GetAll();

            foreach(var account in accounts)
            {
                if (predicate(account))
                {
                    return account;
                }
            }

            return null;
        }

        public void Clear()
        {
            File.WriteAllText(filePath, string.Empty);
        }
    }
}
